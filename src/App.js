import React, { Component } from "react";
import axios from "axios";
import GameList from "./components/GameList";
import SearchBar from "./components/SearchBar";
import SortOptions from "./components/SortOptions";
import Footer from "./components/Footer";
import Header from "./components/Header";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      allGames: [],
      games: [],
      sort: "",
      limit: 50,
    };

    this.getGamesByText = this.getGamesByText.bind(this);
    this.getGamesBySort = this.getGamesBySort.bind(this);
    this.increaseLoadLimit = this.increaseLoadLimit.bind(this);
  }

  componentDidMount() {
    axios.get("/games").then((res) => {
      const list = res.data;
      this.setState({
        allGames: list,
        games: list,
      });
    });
  }

  getGamesByText(text) {
    text = text.toLowerCase();
    const newList = this.state.allGames.filter((game) => {
      return game.title.toString().toLowerCase().includes(text);
    });

    this.setState({
      games: newList,
      limit: 50,
    });
  }

  getGamesBySort(param) {
    const newList = this.state.allGames.sort((a, b) => param === "1" ? a.score - b.score : b.score - a.score);

    this.setState({
      allGames: newList,
      games: newList,
    });
  }

  increaseLoadLimit() {
    this.setState((state) => {
      return {
        limit: Math.min(state.limit + 50, state.allGames.length),
      };
    });
  }

  render() {
    return (
      <div className="">
        <Header />
        <section className="section" style={{ height: "100vh" }}>
          <div className="container">
            <div className="columns">
              <div className="column is-one-third">
                <SearchBar handleInput={this.getGamesByText} />
                <SortOptions handleSortChange={this.getGamesBySort} />
              </div>
              <div className="column has-background-grey-lighter">
                <GameList
                  games={this.state.games.slice(0, this.state.limit)}
                  handleLoadMore={this.increaseLoadLimit}
                />
              </div>
            </div>
          </div>
        </section>
        <Footer />
      </div>
    );
  }
}

export default App;
