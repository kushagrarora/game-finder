import React from "react";
import PropTypes from "prop-types";

function GameCard(props) {
  const { game } = props;

  const cardStyle = {
    margin: "0 16px 16px",
    display: "inline-block",
    width: document && document.body && document.body.clientWidth < 768 ? "100%" : "45%",
    height: "300px",
    overflow: "hidden",
    position: "relative"
  };

  const btnStyle = {
    position: "absolute",
    bottom: "1.5rem",
    left: "1.5rem",
  };

  const editorStyle = {
    position: "absolute",
    bottom: "1.5rem",
    right: "1.5rem",
  };

  const scoreStyle = {
    position: "absolute",
    top: "50%",
    right: "1.5rem",
    display: "inline-block",
    borderRadius: "50%",
    padding: "2px",
    background: "rgb(12, 102, 161)",
    width: "28px",
    color: "yellow",
    textAlign: "center",
    fontWeight: "bold",
    fontSize: "12px",
    height: "28px",
    lineHeight: "24px"
  };

  return (
    <article className="card" style={cardStyle}>
      <div className="card-content">
        <h3 className="is-size-4">{game.title}</h3>
        <hr/>
        <p>Platform: <strong>{game.platform}</strong></p>
        <p>Genre: <strong>{game.genre}</strong></p>
        <p>Release Year: <strong>{game.release_year}</strong></p>
        <span style={scoreStyle}>
          {game.score}
        </span>
        {
          game.editors_choice === "Y" && 
          <div style={editorStyle}>
            <span className="has-text-danger">❤︎</span>
            {' '} Editor's Fav
          </div>
        }
        <div className="buttons" style={btnStyle}>
          <a className="button is-success is-small" href={game.url}>Play Now</a>
        </div>
      </div>
    </article>
  );
}

GameCard.propTypes = {
  game: PropTypes.object,
  color: PropTypes.string,
};

export default GameCard;
