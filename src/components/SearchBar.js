import React, { useState } from "react";
import PropTypes from "prop-types";

function SearchBar(props) {
  const { handleInput } = props;

  const [text, setText] = useState("");

  return (
    <div className="field is-grouped">
      <p className="control is-expanded">
        <input
          className="input"
          type="text"
          placeholder="Find a game"
          value={text}
          onChange={(ev) => setText(ev.target.value)}
        />
      </p>
      <p className="control">
        <button className="button is-info" onClick={() => handleInput(text)}>
          Search
        </button>
      </p>
    </div>
  );
}

SearchBar.propTypes = {
  handleInput: PropTypes.func,
};

export default SearchBar;
