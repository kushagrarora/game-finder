import React from 'react'
import PropTypes from 'prop-types'

function SortOptions(props) {
    return (
      <div>
        <div className="control">
          <label className="radio">
            <input type="radio" name="sort" onChange={ev => props.handleSortChange(ev.target.value)} value="1"/>
            {" "} Sort by score - Low to High
          </label>
        </div>
        <div className="control">
          <label className="radio">
            <input type="radio" name="sort" onChange={ev => props.handleSortChange(ev.target.value)} value="-1"/>
            {" "} Sort by score - High to Low
          </label>
        </div>
      </div>
    )
}

SortOptions.propTypes = {
  handleSortChange : PropTypes.func
}

export default SortOptions

