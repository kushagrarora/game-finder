import React from "react";
import PropTypes from "prop-types";
import GameCard from "./GameCard";

function GameList(props) {
  const { games, handleLoadMore } = props;

  const listContainer = {
    height: "calc(100vh - 48px)",
    overflow: "hidden",
    overflowY: "auto",
  };

  return (
    <div style={listContainer}>
      <div className="">
        {games.map((game, index) => (
          <GameCard key={game.url + index} game={game} />
        ))}
      </div>

      <div className="buttons">
        <button
          className="button is-primary"
          onClick={handleLoadMore}
          style={{ marginLeft: "auto", marginRight: "auto" }}>
          Load More
        </button>
      </div>
    </div>
  );
}

GameList.propTypes = {
  games: PropTypes.array,
};

export default GameList;
