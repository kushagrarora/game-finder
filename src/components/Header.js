import React from "react";

export default function Header() {
  return (
    <section className="hero is-medium is-primary is-bold">
      <div className="hero-body">
        <div className="container">
          <h1 className="title">Tesla Games</h1>
          <h2 className="subtitle">Best games to play during lockdown</h2>
        </div>
      </div>
    </section>
  );
}
